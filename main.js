const express = require('express');
const port = 3000;
const app = express();

const MIN = 800;
const MAX = 2000;

app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    })
    .listen(port, () => {
    console.log('Server started on port: ', port);
});


app.get('/', (req, res) => {
    res.send('App work');
});

app.get('/left-servo', (req, res) => {
    const position = req.query.position;
    const pulse = degreeToPulse(position);
    // moveLeftServo(pulse);
    console.log('Left', pulse);
    res.status(200).send('result');
});

app.get('/right-servo', (req, res) => {
    const position = req.query.position;
    const pulse = degreeToPulse(position);
    // moveRightServo(pulse);
    console.log('Right', pulse);
    res.status(200).send('result');
});

app.get('/base-servo', (req, res) => {
    const position = req.query.position;
    const pulse = degreeToPulse(position);
    // moveBase(pulse);
    console.log('Base', pulse);
    res.status(200).send('result');
});

app.get('/pump', (req, res) => {
    console.log('Req params: ', req.query.on);
    res.send('base servo moved');
});

app.get('/scan', (req, res) => {

});

// -helper

function degreeToPulse(value) { // 180 -> 1900
    const operatedValue = getOperatedValue(value);
    const pulse = getPulseWidth(operatedValue);
    return pulse;
}

function getOperatedValue(value) {
    if (value > 180) return 180;
    if (value < 0) return 0;
    return value;
}

function getPulseWidth(pulse) {
    const pulseWidth = Math.round(((pulse * (MAX - MIN)) / 180) + MIN);
    return pulseWidth;
}


// servoadapter

const Gpio = require('pigpio').Gpio;

function moveServo(servo) {
    const pulse = degreeToPulse(servo.currentPosition);
    servo.motor.servoWrite(pulse);
}

// -------

const readline = require('readline');

const step = 5;    // degree
const delay = 50; // ms

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});


function requestPosition() {
    return new Promise((resolve) => {
        rl.question(`Position: `, (value) => {
            resolve(value);
        });
    });
}

function smoothMove(pos, servo) {
    var position = Number(pos);
    // standart moving
    if (Math.abs(position - servo.currentPosition) !== 0 && position >= servo.minimalPosition && position <= servo.maximumPosition) {
        const way = position < servo.currentPosition ? -1 : 1;
        intervalStart(position, servo, way);
    } else {
        // position > maximum
        if (Math.abs(position - servo.currentPosition) !== 0 && position > servo.maximumPosition) {
            position = servo.maximumPosition;
            intervalStart(position, servo, 1);
        }
        // position < minimum
        if (Math.abs(position - servo.currentPosition) !== 0 && position < servo.minimalPosition) {
            position = servo.minimalPosition;
            intervalStart(position, servo, -1);
        }
        // position == current
        if (Math.abs(position - servo.currentPosition) == 0) {
            requestPosition().then(position => smoothMove(position, servo));
        }
    }
}


function initializeServo(min, max, current) {
    const minimalPosition = min;
    const maximumPosition = max;
    var currentPosition = current;
    
    
    return function(pin) {
        const motor = new Gpio(pin, {mode: Gpio.OUTPUT});
        return {
            minimalPosition,
            maximumPosition,
            currentPosition,
            motor
        };
    }
}

function intervalStart(position, servo, way) {
    const interval = setInterval(() => {
        if (position !== servo.currentPosition) {
            servo.currentPosition += way * step;
            moveServo(servo);
        } else {
            console.log('Stop');
            clearInterval(interval);
            requestPosition().then(position => smoothMove(position, servo));
        }
    }, delay);
}

const height = 40; // degree
const fill = 1000; // sec
const cups = [
    {
        degree: 10,
        distance: 50
    },
    {
        degree: 25,
        distance: 40
    },
    {
        degree: 90,
        distance: 60
    },
    {
        degree: 160,
        distance: 40
    }
];



const rightServoSettings = initializeServo(0, 90, 0); // height
const baseServoSettings = initializeServo(0, 180, 0); // degree
const leftServoSettings = initializeServo(0, 80, 0);  // distance
const rightServo = rightServoSettings(21);
const baseServo = baseServoSettings(16);
const leftServo = leftServoSettings(20);
requestPosition().then(position => fillAll());

function fillAll() {
    smoothMove(0, rightServo);
    smoothMove(0, baseServo);
}


